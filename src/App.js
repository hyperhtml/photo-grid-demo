import { StrictMode } from 'react';
import {Container} from '@mui/material';

import PhotoGrid from './components/PhotoGrid';
import images from './sample_images.json'

function App() {
  return (
    <StrictMode>
      <Container maxWidth="xl">
        <PhotoGrid photos={images.map(({_id: id, href, dimensions: {h, w}})=>({
          src: href,
          width: w,
          height: h,
          id,
        }))} />
      </Container>
    </StrictMode>
  );
}

export default App;
