import { useEffect, useRef, useState, useMemo } from "react";
import { Box } from '@mui/material';
import justifiedLayout from "justified-layout";

const PhotoGrid = ({ photos }) => {
    const ref = useRef(null);
    const [viewportWidth, setViewportWidth] = useState(ref.current ? ref.current.offsetWidth : 0);

    useEffect(() => {
        const handleResize = () => {
            setViewportWidth(ref.current ? ref.current.offsetWidth : 0);
        };
        window.addEventListener('resize', handleResize);
        
        return () => {
          window.removeEventListener('resize', handleResize);
        };
      }, []);

    useEffect(() => {
        setViewportWidth(ref.current ? ref.current.offsetWidth : 0);
    },[])

    const photoLayout = useMemo(() => justifiedLayout(photos, {containerWidth: viewportWidth}), [photos, viewportWidth]);
    return (
        <Box ref={ref} style={{position: "relative"}} sx={{
            '.placeholder': {

            }
        }}>
            {photoLayout.boxes.map(({top, width, height, left}, index)=>{
                const { id, src} = photos[index];

                return (
                    <img key={id} className="placeholder" src={src} height={height} width={width} style={{top: top, left: left, position: "absolute"}} loading="lazy" alt="placeholder text" />
                );
            })}
            
        </Box>
    )
};

export default PhotoGrid;
