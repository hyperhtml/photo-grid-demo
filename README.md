# Photo Grid Component Demo

## Setup

1. `npm i`
2. `npm start`

## Test

`npm run test` - Basic test of rendering component

## References and Libraries Used

[Google Design Blog - Google Photos](https://medium.com/google-design/google-photos-45b714dfbed1) - Technical write-up on how Google accomplished some of the challenges with a justified gallery layout. Led me to Flickr's open source library [justified-layout](https://github.com/flickr/justified-layout).

[justified-layout](https://github.com/flickr/justified-layout) - Used on flickr for the gallery views.

