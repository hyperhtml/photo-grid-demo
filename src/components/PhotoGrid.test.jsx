import { render, screen } from "@testing-library/react";
import PhotoGrid from "./PhotoGrid";

describe("PhotoGrid", () => {
    it("should render properly", () => {
        render(<PhotoGrid photos={[{src: "url", height: 1, width: 1}]} />);

        expect(screen.getByAltText("placeholder text").getAttribute('src')).toEqual("url");
    });
});